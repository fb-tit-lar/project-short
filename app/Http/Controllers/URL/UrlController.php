<?php


namespace App\Http\Controllers\URL;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\URLShort;
class UrlController extends Controller
{
    public function short(Request $request)
    {
        //TODO: check url exists ?
        $url = URLShort::whereUrl($request->url)->first();
        //dd($request->all());

        //If url exists then just show the short url

        // else generate a short url and save to DB and display url
        if ($url == null) {
            $short = $this->generateShortURL();
            URLShort::create([
                'url' => $request->url,
                'short' => $short
            ]);

            $url = URLShort::whereUrl($request->url)->first();

        }

        return view('url.short_url', compact('url'));
    }

    public function shortLink($link)
    {
        $url = URLShort::whereShort($link)->first();
        return redirect($url->url);
    }

    public function generateShortURL()
    {
        $result = (rand(10000, 99999) . chr(rand(65, 90)));
        $data = URLShort::whereShort($result)->first();

        if ($data != null) {
            $this->generateShortURL();
        }

        session()->flash('notif', 'Success to save ' . "www.short.local/" . $result); //notification
        return $result;

    }
}
//
//namespace App\Http\Controllers\URL;
//
//use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
//
//use App\Model\URLShort;
//
//class UrlController extends Controller
//{
//    public function short(Request $request){
//        //TODO: check url exists ?
//        $url = URLShort::whereUrl($request->url)->first();
//        //dd($request->all());
//
//        //If url exists then just show the short url
//
//        // else generate a short url and save to DB and display url
//        if($url == null){
//            $short = $this->generateShortURL();
//            URLShort::create([
//                'url' => $request->url,
//                'short' => $short
//            ]);
//
//            $url = URLShort::whereUrl($request->url)->first();
//
//        }
//
//        return view('url.short_url',compact('url'));
//    }
//    public function generateShortURL(){
//        $result = (rand(10000,99999).chr(rand(65,90)));
//        $data = URLShort::whereShort($result)->first();
//
//        if($data != null){
//            $this->generateShortURL();
//        }
//
////        session()->flash('notif','Success to save '."www.short.local/".$result); //notification
//        return $result;
//
//    }
//}
